# kmovies

Aplikasi untuk menonton film dan tv.

## How to Run

- git clone https://gitlab.com/test-kerja1/movies-dan-tv-shows.git
- open folder
- run "flutter pub get" in terminal
- run "flutter run" in terminal

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
