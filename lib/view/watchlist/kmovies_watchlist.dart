import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kmovies/controller/kmovies_home_controller.dart';
import 'package:kmovies/helpers/constant.dart';
import 'package:kmovies/helpers/drawable.dart';

class KMoviesWatchlist extends StatefulWidget {
  const KMoviesWatchlist({Key? key}) : super(key: key);

  @override
  _KMoviesWatchlistState createState() => _KMoviesWatchlistState();
}

class _KMoviesWatchlistState extends State<KMoviesWatchlist> {
  ScreenUtil _dimens = ScreenUtil();
  ScrollController _scrollController = new ScrollController();

  final _KHomeController = Get.find<KHomeController>();

  bool selectedTabs = true;

  @override
  void initState() {
    super.initState();
    handleScroll();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Stack(
          children: [
            ClipPath(
              clipper: OvalBottomBorderClipper(),
              child: Container(
                height: _dimens.setHeight(120.0),
                color: constants.primaryColor,
                child: Center(
                    child: Text('Watchlist', style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: _dimens.setSp(18.0)),)
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: _dimens.setHeight(39.0), left: _dimens.setWidth(20.0)),
              child: tombolAtas(
                Icons.arrow_back_outlined,
                20.0,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: _dimens.setHeight(120.0)),
              margin: EdgeInsets.symmetric(
                  horizontal: _dimens.setWidth(20.0),
                  vertical: _dimens.setHeight(20.0)),
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                        height: _dimens.setHeight(45.0),
                        child: ElevatedButton(
                          child: Text(
                            'Movies',
                            style: TextStyle(
                                fontFamily: 'Nunito',
                                fontSize: _dimens.setSp(14.0),
                              color: Colors.black
                            ),
                          ),
                          onPressed: () async {
                            setState(() {
                              selectedTabs = true;
                            });
                          },
                          style: ElevatedButton.styleFrom(
                            primary: selectedTabs
                                ? constants.primaryColor
                                : Color(0xFFF0F0F0),
                            onPrimary: selectedTabs
                                ? Color(0xFF004887)
                                : Color(0xFFA2A2A2),
                            side: BorderSide(
                                color:
                                selectedTabs ? constants.primaryColor : Colors.transparent,
                                width: selectedTabs ? 2 : 0),
                            textStyle: TextStyle(
                                fontSize: _dimens.setSp(16.0),
                                fontWeight: FontWeight.bold),
                            elevation: 1,
                            shadowColor: Colors.grey,
                            padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                      )),
                  SizedBox(
                    width: _dimens.setWidth(13.0),
                  ),
                  Expanded(
                      child: Container(
                        height: _dimens.setHeight(45.0),
                        child: ElevatedButton(
                          child: Text(
                            'Tv Shows',
                            style: TextStyle(
                                fontFamily: 'Nunito',
                                fontSize: _dimens.setSp(14.0),
                                color: Colors.black),
                          ),
                          onPressed: () async {
                            setState(() {
                              selectedTabs = false;
                            });
                            // onRefreshList();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: selectedTabs
                                ? Color(0xFFF0F0F0)
                                : constants.primaryColor,
                            onPrimary: selectedTabs
                                ? Color(0xFFA2A2A2)
                                : Color(0xFF004887),
                            side: BorderSide(
                                color:
                                selectedTabs ? Colors.transparent : constants.primaryColor,
                                width: selectedTabs ? 0 : 2),
                            textStyle: TextStyle(
                                fontSize: _dimens.setSp(16.0),
                                fontWeight: FontWeight.bold),
                            elevation: 1,
                            shadowColor: Colors.grey,
                            padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                      ),),
                ],
              ),
            ),
            selectedTabs ? Container(
              padding: EdgeInsets.only(top: _dimens.setHeight(140.0), left: _dimens.setWidth(20.0), right: _dimens.setWidth(20.0)),
              height: MediaQuery.of(context).size.height / 1,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Center(
                      child: Image.asset(
                        DrawableX.imageAsset(AssetGambar.null_file),
                        width: _dimens.setWidth(250.0),
                      ),
                    ),
                    SizedBox(
                      height: _dimens.setHeight(20.0),
                    ),
                    Text(
                      'Belum ada Movies di Watchlist',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Nunito',
                        fontSize: _dimens.setSp(16.0),
                      ),
                    ),
                    SizedBox(
                      height: _dimens.setHeight(20.0),
                    ),
                    FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(12.0),
                        side: BorderSide(color: constants.primaryColor),
                      ),
                      color: Colors.white,
                      child: Text('Coba lagi',
                          style: TextStyle(color: constants.primaryColor)),
                      onPressed: null,
                    ),
                  ],
                ),
              ),
            ) : Container(
              padding: EdgeInsets.only(top: _dimens.setHeight(140.0), left: _dimens.setWidth(20.0), right: _dimens.setWidth(20.0)),
              height: MediaQuery.of(context).size.height / 1,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Center(
                      child: Image.asset(
                        DrawableX.imageAsset(AssetGambar.null_file),
                        width: _dimens.setWidth(250.0),
                      ),
                    ),
                    SizedBox(
                      height: _dimens.setHeight(20.0),
                    ),
                    Text(
                      'Belum ada Tv Shows di Watchlist',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Nunito',
                        fontSize: _dimens.setSp(16.0),
                      ),
                    ),
                    SizedBox(
                      height: _dimens.setHeight(20.0),
                    ),
                    FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(12.0),
                        side: BorderSide(color: constants.primaryColor),
                      ),
                      color: Colors.white,
                      child: Text('Coba lagi',
                          style: TextStyle(color: constants.primaryColor)),
                      onPressed: null,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget tombolAtas(icon, iconSize) {
    return GestureDetector(
      onTap: () {
        Get.back();
      },
      child: Container(
        width: _dimens.setWidth(35.0),
        height: _dimens.setHeight(35.0),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(1, 1), // changes position of shadow
              )
            ],
            //     border: Border.all(color: Colors.grey, width: 0),
            color: Colors.white,
            shape: BoxShape.circle),
        child: Icon(
          icon,
          size: iconSize,
          color: Colors.black,
        ),
      ),
    );
  }

  void handleScroll() async {
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        _KHomeController.navAutoHide.value = false;
        // Logger.logMe('navAutoHide reverse : ' + _userController.navAutoHide.value.toString());
      }
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        _KHomeController.navAutoHide.value = true;
        // Logger.logMe('navAutoHide forward : ' + _userController.navAutoHide.value.toString());
      }
    });
  }
}
