import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kmovies/controller/kmovies_home_controller.dart';
import 'package:kmovies/helpers/constant.dart';
import 'package:kmovies/helpers/drawable.dart';
import 'package:kmovies/helpers/widget/widget_movies_grid.dart';
import 'package:kmovies/models/Response/ResModelsNowPlayingMovies.dart';
import 'package:kmovies/models/Response/ResModelsPopularMovies.dart';
import 'package:kmovies/models/Response/ResModelsPopularTvShows.dart';
import 'package:kmovies/models/Response/ResModelsTopRatedMovies.dart';
import 'package:kmovies/models/Response/ResModelsTopRatedTvShows.dart';
import 'package:kmovies/models/Response/ResModelsUpcomingMovies.dart';
import 'package:kmovies/services/base_url.dart';
import 'package:kmovies/view/global/kmovies_search.dart';
import 'package:kmovies/view/watchlist/kmovies_watchlist.dart';

class KMoviesHome extends StatefulWidget {
  const KMoviesHome({Key? key}) : super(key: key);

  @override
  _KMoviesHomeState createState() => _KMoviesHomeState();
}

class _KMoviesHomeState extends State<KMoviesHome> {
  ScreenUtil _dimens = ScreenUtil();
  ScrollController _scrollController = new ScrollController();

  final _KHomeController = Get.find<KHomeController>();

  @override
  void initState() {
    onRefresh();
    super.initState();
    handleScroll();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: ()=> onRefresh(),
        child: CustomScrollView(
          controller: _scrollController,
          physics: AlwaysScrollableScrollPhysics(),
          slivers: [
            SliverList(delegate: SliverChildListDelegate([
            Stack(
            children: [
            ClipPath(
            clipper: OvalBottomBorderClipper(),
              child: Container(
                height: _dimens.setHeight(150.0),
                color: constants.primaryColor,
                child: Center(
                    child: Image.asset(DrawableX.imageAsset(AssetGambar.logo_dark), width: _dimens.setWidth(130.0),)
                ),
              ),
            ),
              Container(
                padding: EdgeInsets.only(
                    top: _dimens.setHeight(120.0),
                    left: _dimens.setWidth(10.0)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: GestureDetector(
                        child: Container(
                          height: _dimens.setHeight(50.0),
                          margin: EdgeInsets.symmetric(
                              horizontal: _dimens.setWidth(20.0)),
                          padding: EdgeInsets.symmetric(
                              horizontal: _dimens.setWidth(20.0)),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset:
                                Offset(0, 2), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.search,
                                color: constants.greyColor,
                                size: _dimens.setWidth(23.0),
                              ),
                              SizedBox(
                                width: _dimens.setWidth(10.0),
                              ),
                              Expanded(
                                child: GestureDetector(
                                  onTap: () async {
                                    _KHomeController.listSearch.clear();
                                    var mResult = await Get.to(KMoviesSearch(lastSearch: _KHomeController.lastsearch.value));
                                    setState(() {
                                      if(mResult['finish']==true){
                                        _KHomeController.lastsearch.value = mResult['lastsearch'];
                                      }else{
                                        _KHomeController.lastsearch.value = 'Cari Buku...';
                                      }
                                    });
                                  },
                                  behavior: HitTestBehavior.opaque,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        _KHomeController.lastsearch.value!='' ? _KHomeController.lastsearch.value : 'Cari ....',
                                        style: TextStyle(
                                            fontFamily: 'Nunito',
                                            color: constants.greyColor),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: _dimens.setWidth(5.0),
                    ),
                    GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          Get.to(KMoviesWatchlist());
                        },
                        child: Image.asset(DrawableX.imageAsset(AssetGambar.watchlist))
                    ),
                    SizedBox(
                      width: _dimens.setWidth(30.0),
                    ),
                  ],
                ),
              ),
            ]),
              Container(
                padding: EdgeInsets.only(left: _dimens.setWidth(15.0)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Obx((){
                      if(_KHomeController.isFetchingTopRated.value==Status.LOADING || _KHomeController.isFetchingUpcoming.value==Status.LOADING
                          || _KHomeController.isFetchingNowPlaying.value==Status.LOADING || _KHomeController.isFetchingPopular.value==Status.LOADING
                          || _KHomeController.isFetchingPopularTv.value==Status.LOADING || _KHomeController.isFetchingTopRatedTv.value==Status.LOADING){
                        return Container(padding: EdgeInsets.symmetric(vertical: _dimens.setHeight(150.0)),child: Align(alignment: Alignment.center,child: CircularProgressIndicator(color: Colors.grey,)));
                      }else if(_KHomeController.isFetchingTopRated.value==Status.ERROR){
                        return Container(padding: EdgeInsets.symmetric(vertical: _dimens.setHeight(150.0)),child: Align(alignment: Alignment.center,child: CircularProgressIndicator(color: Colors.grey,)));
                      }else{
                        return Container();
                      }
                    }),
                    Obx(
                          () => wgtListTopRatedMovies(
                          title: 'Top Rated',
                          result: _KHomeController.listTopRated,
                          status:
                          _KHomeController.isFetchingTopRated.value),
                    ),
                    Obx(
                          () => wgtListUpcomingMovies(
                          title: 'Upcoming',
                          result: _KHomeController.listUpcoming,
                          status:
                          _KHomeController.isFetchingUpcoming.value),
                    ),
                    Obx(
                          () => wgtListNowPlayingMovies(
                          title: 'Now Playing',
                          result: _KHomeController.listNowPlaying,
                          status:
                          _KHomeController.isFetchingNowPlaying.value),
                    ),
                    Obx(
                          () => wgtListPopularMovies(
                          title: 'Popular',
                          result: _KHomeController.listPopular,
                          status:
                          _KHomeController.isFetchingPopular.value),
                    ),
                    Obx(
                          () => wgtListPopularTvShows(
                          title: 'Popular Tv Shows',
                          result: _KHomeController.listPopularTv,
                          status:
                          _KHomeController.isFetchingPopularTv.value),
                    ),
                    Obx(
                          () => wgtListTopRatedTvShows(
                          title: 'Top Rated Tv Shows',
                          result: _KHomeController.listTopRatedTv,
                          status:
                          _KHomeController.isFetchingTopRatedTv.value),
                    ),
                    SizedBox(height: _dimens.setHeight(30.0),)
                  ],
                ),
              ),
            ]))
          ],
        ),
      ),
    );
  }

  Widget wgtListTopRatedMovies(
      {@required String? title,
        @required List<ResultsTopRated>? result,
        @required Status? status}) {
    return Container(
      // color: Colors.grey.withOpacity(0.2),
      margin: EdgeInsets.only(top: _dimens.setHeight(30.0)),
      child: status == Status.LOADING
          ? Container()
          : result!.length > 0
          ? Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: _dimens.setWidth(20.0),
                right: _dimens.setWidth(20.0)),
            child: Text(
              title!,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: _dimens.setSp(16.0),
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: _dimens.setWidth(20.0)),
            height: _dimens.setHeight(280.0),
            // color: Colors.blueAccent,
            child: ListView.builder(
              itemCount: result.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: _dimens.setHeight(10.0)),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var listData = result[index];
                return Container(
                  padding:
                  EdgeInsets.only(right: _dimens.setWidth(5.0)),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.10),
                        blurRadius: 20.0, // soften the shadow
                        spreadRadius: 0.0, //extend the shadow
                        offset: Offset(
                          5.0, // Move to right 10  horizontally
                          5.0, // Move to bottom 10 Vertically
                        ),
                      )
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: MoviesTopRatedItem(
                        listData: listData,
                        index: index,
                        lastIndex: result.length,
                        dimens: _dimens,
                        title: title,
                        listView: true,
                        onTap: () {
                        }),
                  ),
                );
              },
            ),
          )
        ],
      )
          : Container()
    );
  }

  Widget wgtListUpcomingMovies(
      {@required String? title,
        @required List<ResultsUpcoming>? result,
        @required Status? status}) {
    return Container(
      // color: Colors.grey.withOpacity(0.2),
      margin: EdgeInsets.only(top: _dimens.setHeight(30.0)),
      child: status == Status.LOADING
          ? Container()
          : result!.length > 0
          ? Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: _dimens.setWidth(20.0),
                right: _dimens.setWidth(20.0)),
            child: Text(
              title!,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: _dimens.setSp(16.0),
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: _dimens.setWidth(20.0)),
            height: _dimens.setHeight(280.0),
            // color: Colors.blueAccent,
            child: ListView.builder(
              itemCount: result.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: _dimens.setHeight(10.0)),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var listData = result[index];
                return Container(
                  padding:
                  EdgeInsets.only(right: _dimens.setWidth(5.0)),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.10),
                        blurRadius: 20.0, // soften the shadow
                        spreadRadius: 0.0, //extend the shadow
                        offset: Offset(
                          5.0, // Move to right 10  horizontally
                          5.0, // Move to bottom 10 Vertically
                        ),
                      )
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: MoviesUpcomingItem(
                        listData: listData,
                        index: index,
                        lastIndex: result.length,
                        dimens: _dimens,
                        title: title,
                        listView: true,
                        onTap: () {
                        }),
                  ),
                );
              },
            ),
          )
        ],
      )
          : Container()
    );
  }

  Widget wgtListNowPlayingMovies(
      {@required String? title,
        @required List<ResultsNowPlaying>? result,
        @required Status? status}) {
    return Container(
      // color: Colors.grey.withOpacity(0.2),
      margin: EdgeInsets.only(top: _dimens.setHeight(30.0)),
      child: status == Status.LOADING
          ? Container()
          : result!.length > 0
          ? Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: _dimens.setWidth(20.0),
                right: _dimens.setWidth(20.0)),
            child: Text(
              title!,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: _dimens.setSp(16.0),
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: _dimens.setWidth(20.0)),
            height: _dimens.setHeight(280.0),
            // color: Colors.blueAccent,
            child: ListView.builder(
              itemCount: result.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: _dimens.setHeight(10.0)),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var listData = result[index];
                return Container(
                  padding:
                  EdgeInsets.only(right: _dimens.setWidth(5.0)),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.10),
                        blurRadius: 20.0, // soften the shadow
                        spreadRadius: 0.0, //extend the shadow
                        offset: Offset(
                          5.0, // Move to right 10  horizontally
                          5.0, // Move to bottom 10 Vertically
                        ),
                      )
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: MoviesNowPlayingItem(
                        listData: listData,
                        index: index,
                        lastIndex: result.length,
                        dimens: _dimens,
                        title: title,
                        listView: true,
                        onTap: () {
                        }),
                  ),
                );
              },
            ),
          )
        ],
      )
          : Container()
    );
  }

  Widget wgtListPopularMovies(
      {@required String? title,
        @required List<ResultsPopular>? result,
        @required Status? status}) {
    return Container(
      // color: Colors.grey.withOpacity(0.2),
      margin: EdgeInsets.only(top: _dimens.setHeight(30.0)),
      child: status == Status.LOADING
          ? Container()
          : result!.length > 0
          ? Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: _dimens.setWidth(20.0),
                right: _dimens.setWidth(20.0)),
            child: Text(
              title!,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: _dimens.setSp(16.0),
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: _dimens.setWidth(20.0)),
            height: _dimens.setHeight(280.0),
            // color: Colors.blueAccent,
            child: ListView.builder(
              itemCount: result.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: _dimens.setHeight(10.0)),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var listData = result[index];
                return Container(
                  padding:
                  EdgeInsets.only(right: _dimens.setWidth(5.0)),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.10),
                        blurRadius: 20.0, // soften the shadow
                        spreadRadius: 0.0, //extend the shadow
                        offset: Offset(
                          5.0, // Move to right 10  horizontally
                          5.0, // Move to bottom 10 Vertically
                        ),
                      )
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: MoviesPopularItem(
                        listData: listData,
                        index: index,
                        lastIndex: result.length,
                        dimens: _dimens,
                        title: title,
                        listView: true,
                        onTap: () {
                        }),
                  ),
                );
              },
            ),
          )
        ],
      )
          : Container()
    );
  }

  Widget wgtListPopularTvShows(
      {@required String? title,
        @required List<ResultsPopularTvShows>? result,
        @required Status? status}) {
    return Container(
      // color: Colors.grey.withOpacity(0.2),
      margin: EdgeInsets.only(top: _dimens.setHeight(30.0)),
      child: status == Status.LOADING
          ? Container()
          : result!.length > 0
          ? Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: _dimens.setWidth(20.0),
                right: _dimens.setWidth(20.0)),
            child: Text(
              title!,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: _dimens.setSp(16.0),
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: _dimens.setWidth(20.0)),
            height: _dimens.setHeight(280.0),
            // color: Colors.blueAccent,
            child: ListView.builder(
              itemCount: result.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: _dimens.setHeight(10.0)),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var listData = result[index];
                return Container(
                  padding:
                  EdgeInsets.only(right: _dimens.setWidth(5.0)),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.10),
                        blurRadius: 20.0, // soften the shadow
                        spreadRadius: 0.0, //extend the shadow
                        offset: Offset(
                          5.0, // Move to right 10  horizontally
                          5.0, // Move to bottom 10 Vertically
                        ),
                      )
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: MoviesPopularTvShowsItem(
                        listData: listData,
                        index: index,
                        lastIndex: result.length,
                        dimens: _dimens,
                        title: title,
                        listView: true,
                        onTap: () {
                        }),
                  ),
                );
              },
            ),
          )
        ],
      )
          : Container()
    );
  }

  Widget wgtListTopRatedTvShows(
      {@required String? title,
        @required List<ResultsTopRatedTv>? result,
        @required Status? status}) {
    return Container(
      // color: Colors.grey.withOpacity(0.2),
      margin: EdgeInsets.only(top: _dimens.setHeight(30.0)),
      child: status == Status.LOADING
          ? Container()
          : result!.length > 0
          ? Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: _dimens.setWidth(20.0),
                right: _dimens.setWidth(20.0)),
            child: Text(
              title!,
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: _dimens.setSp(16.0),
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: _dimens.setWidth(20.0)),
            height: _dimens.setHeight(280.0),
            // color: Colors.blueAccent,
            child: ListView.builder(
              itemCount: result.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: _dimens.setHeight(10.0)),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var listData = result[index];
                return Container(
                  padding:
                  EdgeInsets.only(right: _dimens.setWidth(5.0)),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.10),
                        blurRadius: 20.0, // soften the shadow
                        spreadRadius: 0.0, //extend the shadow
                        offset: Offset(
                          5.0, // Move to right 10  horizontally
                          5.0, // Move to bottom 10 Vertically
                        ),
                      )
                    ],
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: MoviesTopRatedTvShowsItem(
                        listData: listData,
                        index: index,
                        lastIndex: result.length,
                        dimens: _dimens,
                        title: title,
                        listView: true,
                        onTap: () {
                        }),
                  ),
                );
              },
            ),
          )
        ],
      )
          : Container()
    );
  }

  void handleScroll() async {
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        _KHomeController.navAutoHide.value = false;
        // Logger.logMe('navAutoHide reverse : ' + _userController.navAutoHide.value.toString());
      }
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        _KHomeController.navAutoHide.value = true;
        // Logger.logMe('navAutoHide forward : ' + _userController.navAutoHide.value.toString());
      }
    });
  }

  Future<void> onRefresh() async {
    _KHomeController.getReqToken();
    _KHomeController.getGuestSession();
    _KHomeController.getTopRatedMovies();
    _KHomeController.getUpcomingMovies();
    _KHomeController.getNowPlayingMovies();
    _KHomeController.getPopularMovies();
    _KHomeController.getPopularTvShows();
    _KHomeController.getTopratedTvShows();
  }
}
