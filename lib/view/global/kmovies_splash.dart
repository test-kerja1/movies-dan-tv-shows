import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kmovies/helpers/constant.dart';
import 'package:kmovies/helpers/drawable.dart';
import 'package:kmovies/helpers/get_storage.dart';
import 'package:kmovies/view/global/bottom_nav.dart';
import 'package:kmovies/view/home/kmovies_home.dart';

class KMoviesSplash extends StatefulWidget {
  const KMoviesSplash({Key? key}) : super(key: key);

  @override
  _KMoviesSplashState createState() => _KMoviesSplashState();
}

class _KMoviesSplashState extends State<KMoviesSplash> {
  ScreenUtil _dimens = ScreenUtil();

  @override
  void initState() {
    Future.delayed(Duration(seconds: 2), () {
      if (getBox.getSplash()!) {
        Get.offAll(KMoviesHome());
      } else {
        goToHome();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: constants.primaryColor,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          goToHome();
        },
        child: Stack(
          children: [
            Positioned.fill(
              top: 0,
              right: 0,
              child: Image.asset(
                DrawableX.imageAsset(AssetGambar.oval_bottom),
                alignment: Alignment.bottomRight,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                DrawableX.imageAsset(AssetGambar.logo),
                width: _dimens.setWidth(252),
              ),
            ),
          ],
        ),
      ),
    );
  }

  goToHome() {
    Get.offAll(KMoviesHome());
  }
}
