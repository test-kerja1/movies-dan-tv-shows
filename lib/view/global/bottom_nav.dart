import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kmovies/controller/kmovies_home_controller.dart';
import 'package:kmovies/helpers/constant.dart';
import 'package:kmovies/helpers/get_storage.dart';
import 'package:kmovies/view/home/kmovies_home.dart';

class BottomNavi extends StatefulWidget {
  final int currentIndex;
  const BottomNavi({Key? key, this.currentIndex = 0}) : super(key: key);

  @override
  _BottomNaviState createState() => _BottomNaviState();
}

class _BottomNaviState extends State<BottomNavi> {
  ScreenUtil _dimens = ScreenUtil();

  var _currentIndex = 0;

  final _KHomeController = Get.find<KHomeController>();

  @override
  void initState() {
    getBox.setSplash(true);
    _currentIndex = widget.currentIndex;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> _children = [
      KMoviesHome(),
    ];

    return Scaffold(
      body: DoubleBackToCloseApp(
        child: _children[_currentIndex],
        snackBar: const SnackBar(
          content: Text('Tap back again to leave'),
        ),
      ),
      bottomNavigationBar: Obx(
            () => AnimatedContainer(
          duration: Duration(milliseconds: 300),
          height: _KHomeController.navAutoHide.value ? _dimens.setHeight(60) : 0,
          child: BottomNaviGate(),
        ),
      ),
    );
  }

  Widget BottomNaviGate() {
    return BottomNavyBar(
      selectedIndex: _currentIndex,
      showElevation: true,
      itemCornerRadius: 24,
      curve: Curves.easeIn,
      onItemSelected: _onItemTap,
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
          activeColor: constants.secondaryColor,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.movie_outlined),
          title: Text('Movies'),
          activeColor: constants.secondaryColor,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.tv),
          title: Text('Tv Shows'),
          activeColor: constants.secondaryColor,
          textAlign: TextAlign.center,
        ),

      ],
    );
  }

  void _onItemTap(int value) {
    setState(() {
        _currentIndex = value;
    });
  }
}
