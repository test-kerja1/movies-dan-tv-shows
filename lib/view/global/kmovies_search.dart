import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kmovies/controller/kmovies_home_controller.dart';
import 'package:kmovies/helpers/widget/widget_global.dart';
import 'package:kmovies/models/Response/ResModelsSearchMoviesTvShows.dart';
import 'package:kmovies/services/base_url.dart';

class KMoviesSearch extends StatefulWidget {
  final String? lastSearch;

  const KMoviesSearch({Key? key, this.lastSearch}) : super(key: key);

  @override
  _KMoviesSearchState createState() => _KMoviesSearchState();
}

class _KMoviesSearchState extends State<KMoviesSearch> {
  ScreenUtil _dimens = ScreenUtil();
  ScrollController _scrollController = new ScrollController();

  final _KHomeController = Get.find<KHomeController>();

  late TextEditingController _textController;
  bool? _wasEmpty;

  @override
  void initState() {
    _textController = TextEditingController(text: widget.lastSearch);
    _wasEmpty = _textController.text.isEmpty;
    _textController.addListener(() {
      if (_wasEmpty != _textController.text.isEmpty) {
        setState(() => {_wasEmpty = _textController.text.isEmpty});
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  height: _dimens.setHeight(110.0),
                  padding: EdgeInsets.only(top: _dimens.setHeight(20.0)),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: _dimens.setWidth(20.0),
                      ),
                      tombolAtas(
                        Icons.arrow_back_outlined,
                        20.0,
                      ),
                      SizedBox(
                        width: _dimens.setWidth(20.0),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(241, 242, 244, 1),
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: TextField(
                            textCapitalization: TextCapitalization.words,
                            controller: _textController,
                            autofocus: true,
                            // textInputAction: TextInputAction.done,
                            onChanged: (text) {
                              setState(() {
                                _KHomeController.lastsearch.value = text;
                                // _textController.text = text;
                              });
                              if(_KHomeController.lastsearch.value!=''){
                                _KHomeController.getSearchMoviesTvShows();
                              }
                            },
                            decoration: InputDecoration(
                              hintStyle: TextStyle(fontSize: _dimens.setSp(14.0)),
                              hintText: 'Cari....',
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                              border: InputBorder.none,
                              contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              suffixIcon: _textController.text.isNotEmpty
                                  ? Padding(
                                padding: const EdgeInsetsDirectional.only(
                                    start: 12.0),
                                child: IconButton(
                                  iconSize: 20.0,
                                  icon: Icon(
                                    Icons.cancel,
                                    color: Colors.grey,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _textController.clear();
                                      _KHomeController.lastsearch.value = '';
                                    });
                                  },
                                ),
                              )
                                  : null,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: _dimens.setWidth(20.0),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: _dimens.setHeight(5.0),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: _dimens.setWidth(20.0)),
                  child: Obx((){
                    if(_KHomeController.isFetchingListSearch.value==Status.LOADING){
                      return CircularProgressIndicator(color: Colors.grey,);
                    }else if(_KHomeController.isFetchingListSearch.value==Status.ERROR){
                      return CircularProgressIndicator(color: Colors.grey,);
                    }else {
                      return _KHomeController.listSearch.length > 0 ? ListView.builder(
                          padding: EdgeInsets.only(top: 1.0),
                          physics: ClampingScrollPhysics(),
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: _KHomeController.listSearch.length,
                          controller: _scrollController,
                          itemBuilder: (BuildContext context, int index) {
                            return bodyFilter(context, _KHomeController.listSearch[index]);
                          }) : Container();
                    }
                  }),
                ),
                SizedBox(
                  height: _dimens.setHeight(20.0),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget bodyFilter(BuildContext context, ResultsSearch list) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: _dimens.setWidth(20.0)),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
        },
        child: Container(
          padding: EdgeInsets.only(top: 20.0),
          height: 100,
          child: Row(
            children: [
              WgtGlobal().imageCache(
                  imgUrl: 'https://image.tmdb.org/t/p/original${list.posterPath}',
                  dimens: _dimens,
                  width: 50.0,
                  height: 120.0,
                  radius: 10.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: _dimens.setWidth(10.0), top: _dimens.setHeight(10.0)),
                      child: Text(
                        '${list.title}',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Nunito',
                            fontSize: _dimens.setSp(14.0),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(left: _dimens.setWidth(10.0), top: _dimens.setHeight(5.0)),
                        child: Text(
                          '${list.releaseDate}',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                              color: Colors.black54,
                              fontFamily: 'Nunito',
                              fontSize: _dimens.setSp(12.0)),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget tombolAtas(icon, iconSize) {
    return GestureDetector(
      onTap: () {
        Get.back(result: {
          "finish": true,
          "lastsearch": "${_KHomeController.lastsearch.value}",
        });
      },
      child: Container(
        width: _dimens.setWidth(35.0),
        height: _dimens.setHeight(35.0),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.8),
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(1, 1), // changes position of shadow
              )
            ],
            //     border: Border.all(color: Colors.grey, width: 0),
            color: Colors.white,
            shape: BoxShape.circle),
        child: Icon(
          icon,
          size: iconSize,
          color: Colors.black,
        ),
      ),
    );
  }
}
