import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kmovies/config/logger.dart';
import 'package:kmovies/helpers/get_storage.dart';
import 'package:kmovies/models/Request/ReqModelsAddToWatchlist.dart';
import 'package:kmovies/models/Request/ReqModelsSession.dart';
import 'package:kmovies/models/Request/ReqModelsValidToken.dart';
import 'package:kmovies/models/Response/ResModelsNowPlayingMovies.dart';
import 'package:kmovies/models/Response/ResModelsPopularMovies.dart';
import 'package:kmovies/models/Response/ResModelsPopularTvShows.dart';
import 'package:kmovies/models/Response/ResModelsSearchMoviesTvShows.dart';
import 'package:kmovies/models/Response/ResModelsTopRatedMovies.dart';
import 'package:kmovies/models/Response/ResModelsTopRatedTvShows.dart';
import 'package:kmovies/models/Response/ResModelsUpcomingMovies.dart';
import 'package:kmovies/services/api_base_helper.dart';
import 'package:kmovies/services/base_url.dart';

class KHomeController extends GetxController {
  var isFetchingTopRated = Status.NETRAL.obs;
  var isFetchingUpcoming = Status.NETRAL.obs;
  var isFetchingNowPlaying = Status.NETRAL.obs;
  var isFetchingPopular = Status.NETRAL.obs;

  var isFetchingPopularTv = Status.NETRAL.obs;
  var isFetchingTopRatedTv = Status.NETRAL.obs;

  var isFetchingAcId = Status.NETRAL.obs;
  var isFetchingListSearch = Status.NETRAL.obs;

  var isFetchingReqToken = Status.NETRAL.obs;
  var isFetchingValidToken = Status.NETRAL.obs;
  var isFetchingSession = Status.NETRAL.obs;
  var isFetchingGuestSession = Status.NETRAL.obs;
  var isFetchingAddToWatchlist = Status.NETRAL.obs;

  var reqBodyAddToWatchlist = ReqModelsAddToWatchlist().obs;

  var reqBodyValidToken = ReqModelsValidToken().obs;
  var reqBodySession = ReqModelsSession().obs;

  var BodyTopRated = ResModelsTopRatedMovies().obs;
  var listTopRated = <ResultsTopRated>[].obs;

  var BodyUpcoming = ResModelsUpcomingMovies().obs;
  var listUpcoming = <ResultsUpcoming>[].obs;

  var BodyNowPlaying = ResModelsNowPlayingMovies().obs;
  var listNowPlaying = <ResultsNowPlaying>[].obs;

  var BodyPopular = ResModelsPopularMovies().obs;
  var listPopular = <ResultsPopular>[].obs;

  var BodyPopularTv = ResModelsPopularTvShows().obs;
  var listPopularTv = <ResultsPopularTvShows>[].obs;

  var BodyTopRatedTv = ResModelsTopRatedTvShows().obs;
  var listTopRatedTv = <ResultsTopRatedTv>[].obs;

  var BodySearch = ResModelsSearchMoviesTvShows().obs;
  var listSearch = <ResultsSearch>[].obs;

  var navAutoHide = true.obs;
  var page = 1.obs;
  var lastsearch = ''.obs;

  getReqToken()async{
    try {
      isFetchingReqToken.value = Status.LOADING;

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetRequestToken());

      if(response['success']!){
        reqBodyValidToken.value = ReqModelsValidToken(username: 'dwijuls',password: '123tester',requestToken: response['request_token']);
        getValidToken();
        isFetchingReqToken.value = Status.COMPLETED;
        xLogger.logMe(log: 'get token = SUCCESS ${response}',tLog: TLog.I);
      }else{
        isFetchingReqToken.value = Status.ERROR;
        xLogger.logMe(log: 'get token = ERROR ${response}',tLog: TLog.E);
        // Get.snackbar('Gagal', '${resultUserProfile.value.message}');
      }
    } catch (e) {
      // Get.snackbar('Gagal', 'Gagal $e ${jsonEncode(resultRegister.value.success)}');
      xLogger.logMe(log: 'get token = ERROR $e',tLog: TLog.E);
      isFetchingReqToken.value = Status.ERROR;
    }
  }

  getValidToken()async{
    try {
      isFetchingValidToken.value = Status.LOADING;

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.post(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetValidToken(), jsonEncode(reqBodyValidToken));

      if(response['success']!){
        reqBodySession.value = ReqModelsSession(requestToken: response['request_token']);
        getSession();
        isFetchingValidToken.value = Status.COMPLETED;
        xLogger.logMe(log: 'valid token = SUCCESS ${response}',tLog: TLog.I);
      }else{
        isFetchingValidToken.value = Status.ERROR;
        xLogger.logMe(log: 'valid token = ERROR ${response}',tLog: TLog.E);
        // Get.snackbar('Gagal', '${resultUserProfile.value.message}');
      }
    } catch (e) {
      // Get.snackbar('Gagal', 'Gagal $e ${jsonEncode(resultRegister.value.success)}');
      xLogger.logMe(log: 'valid token = ERROR $e',tLog: TLog.E);
      isFetchingValidToken.value = Status.ERROR;
    }
  }

  getSession()async{
    try {
      isFetchingSession.value = Status.LOADING;

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.post(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetSession(), jsonEncode(reqBodySession));

      if(response['success']!){
        getBox.setSessionId(response['session_id']);
        getAccountId();
        isFetchingSession.value = Status.COMPLETED;
        xLogger.logMe(log: 'get session = SUCCESS ${response}',tLog: TLog.I);
      }else{
        isFetchingSession.value = Status.ERROR;
        xLogger.logMe(log: 'get session = ERROR ${response}',tLog: TLog.E);
        // Get.snackbar('Gagal', '${resultUserProfile.value.message}');
      }
    } catch (e) {
      // Get.snackbar('Gagal', 'Gagal $e ${jsonEncode(resultRegister.value.success)}');
      xLogger.logMe(log: 'get session = ERROR $e',tLog: TLog.E);
      isFetchingSession.value = Status.ERROR;
    }
  }

  getAccountId()async{
    try {
      isFetchingAcId.value = Status.LOADING;

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetAccountId());

      xLogger.logMe(log: 'get account id = SUCCESS ${response}',tLog: TLog.I);
      if(response['id']!='' || response['id']!=null){
        getBox.setIdUser(response['id'].toString());
        isFetchingAcId.value = Status.COMPLETED;
        xLogger.logMe(log: 'get account id = SUCCESS ${response}',tLog: TLog.I);
      }else{
        isFetchingAcId.value = Status.ERROR;
        xLogger.logMe(log: 'get account id = ERROR ${response}',tLog: TLog.E);
        // Get.snackbar('Gagal', '${resultUserProfile.value.message}');
      }
    } catch (e) {
      // Get.snackbar('Gagal', 'Gagal $e ${jsonEncode(resultRegister.value.success)}');
      xLogger.logMe(log: 'get account id = ERROR $e',tLog: TLog.E);
      isFetchingAcId.value = Status.ERROR;
    }
  }

  getGuestSession()async{
    try {
      isFetchingGuestSession.value = Status.LOADING;

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetSession());

      if(response['success']!){
        getBox.setGuestSessionId(response['session_id']);
        isFetchingGuestSession.value = Status.COMPLETED;
        xLogger.logMe(log: 'get session = SUCCESS ${response}',tLog: TLog.I);
      }else{
        isFetchingGuestSession.value = Status.ERROR;
        xLogger.logMe(log: 'get session = ERROR ${response}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get session = ERROR $e',tLog: TLog.E);
      isFetchingGuestSession.value = Status.ERROR;
    }
  }

  getTopRatedMovies()async{
    try {
      isFetchingTopRated.value = Status.LOADING;
      listTopRated.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetTopRatedMovies(page.value));
      BodyTopRated.value = ResModelsTopRatedMovies.fromJson(response);

      if(BodyTopRated.value.results!.length>0){
        listTopRated.addAll(BodyTopRated.value.results!);
        isFetchingTopRated.value = Status.COMPLETED;
        xLogger.logMe(log: 'get top rated = SUCCESS ${response['results']}',tLog: TLog.I);
      }else{
        isFetchingTopRated.value = Status.ERROR;
        xLogger.logMe(log: 'get top rated = ERROR ${response['results']}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get top rated = ERROR $e',tLog: TLog.E);
      isFetchingTopRated.value = Status.ERROR;
    }
  }

  getUpcomingMovies()async{
    try {
      isFetchingUpcoming.value = Status.LOADING;
      listUpcoming.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetUpcomingMovies(page.value));
      BodyUpcoming.value = ResModelsUpcomingMovies.fromJson(response);

      if(BodyUpcoming.value.results!.length>0){
        listUpcoming.addAll(BodyUpcoming.value.results!);
        isFetchingUpcoming.value = Status.COMPLETED;
        xLogger.logMe(log: 'get upcoming = SUCCESS ${BodyUpcoming.value.results}',tLog: TLog.I);
      }else{
        isFetchingUpcoming.value = Status.ERROR;
        xLogger.logMe(log: 'get upcoming = ERROR ${BodyUpcoming.value.results}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get upcoming = ERROR $e',tLog: TLog.E);
      isFetchingUpcoming.value = Status.ERROR;
    }
  }

  getNowPlayingMovies()async{
    try {
      isFetchingNowPlaying.value = Status.LOADING;
      listNowPlaying.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetNowPlayingMovies(page.value));
      BodyNowPlaying.value = ResModelsNowPlayingMovies.fromJson(response);

      if(BodyNowPlaying.value.results!.length>0){
        listNowPlaying.addAll(BodyNowPlaying.value.results!);
        isFetchingNowPlaying.value = Status.COMPLETED;
        xLogger.logMe(log: 'get now playing = SUCCESS ${BodyNowPlaying.value.results}',tLog: TLog.I);
      }else{
        isFetchingNowPlaying.value = Status.ERROR;
        xLogger.logMe(log: 'get now playing = ERROR ${BodyNowPlaying.value.results}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get now playing = ERROR $e',tLog: TLog.E);
      isFetchingNowPlaying.value = Status.ERROR;
    }
  }

  getPopularMovies()async{
    try {
      isFetchingPopular.value = Status.LOADING;
      listPopular.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetPopularMovies(page.value));
      BodyPopular.value = ResModelsPopularMovies.fromJson(response);

      if(BodyPopular.value.results!.length>0){
        listPopular.addAll(BodyPopular.value.results!);
        isFetchingPopular.value = Status.COMPLETED;
        xLogger.logMe(log: 'get popular = SUCCESS ${BodyPopular.value.results}',tLog: TLog.I);
      }else{
        isFetchingPopular.value = Status.ERROR;
        xLogger.logMe(log: 'get popular = ERROR ${BodyPopular.value.results}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get popular = ERROR $e',tLog: TLog.E);
      isFetchingPopular.value = Status.ERROR;
    }
  }

  getPopularTvShows()async{
    try {
      isFetchingPopularTv.value = Status.LOADING;
      listPopularTv.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetPopularTvShows(page.value));
      BodyPopularTv.value = ResModelsPopularTvShows.fromJson(response);

      if(BodyPopularTv.value.results!.length>0){
        listPopularTv.addAll(BodyPopularTv.value.results!);
        isFetchingPopularTv.value = Status.COMPLETED;
        xLogger.logMe(log: 'get populartv = SUCCESS ${BodyPopularTv.value.results}',tLog: TLog.I);
      }else{
        isFetchingPopularTv.value = Status.ERROR;
        xLogger.logMe(log: 'get populartv = ERROR ${BodyPopularTv.value.results}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get populartv = ERROR $e',tLog: TLog.E);
      isFetchingPopularTv.value = Status.ERROR;
    }
  }

  getTopratedTvShows()async{
    try {
      isFetchingTopRatedTv.value = Status.LOADING;
      listTopRatedTv.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetTopRatedTvShows(page.value));
      BodyTopRatedTv.value = ResModelsTopRatedTvShows.fromJson(response);

      if(BodyTopRatedTv.value.results!.length>0){
        listTopRatedTv.addAll(BodyTopRatedTv.value.results!);
        isFetchingTopRatedTv.value = Status.COMPLETED;
        xLogger.logMe(log: 'get populartv = SUCCESS ${BodyTopRatedTv.value.results}',tLog: TLog.I);
      }else{
        isFetchingTopRatedTv.value = Status.ERROR;
        xLogger.logMe(log: 'get populartv = ERROR ${BodyTopRatedTv.value.results}',tLog: TLog.E);
      }
    } catch (e) {
      xLogger.logMe(log: 'get populartv = ERROR $e',tLog: TLog.E);
      isFetchingTopRatedTv.value = Status.ERROR;
    }
  }

  getSearchMoviesTvShows()async{
    try{
      isFetchingListSearch.value = Status.LOADING;
      listSearch.clear();

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.get(BaseUrl.BASE_URL_API, BaseUrlHelper.uriGetSearch(lastsearch.value));
      BodySearch.value = ResModelsSearchMoviesTvShows.fromJson(response);
      if(BodySearch.value.results!.length>0){
        listSearch.addAll(BodySearch.value.results!);
        isFetchingListSearch.value = Status.COMPLETED;
        xLogger.logMe(log: 'getData = SUCCESS ${BodySearch.value.results!}', tLog: TLog.I);
      }else{
        isFetchingListSearch.value = Status.ERROR;
        xLogger.logMe(log: 'getData = ERROR ${BodySearch.value.results!}', tLog: TLog.E);
      }
    }catch(e){
      isFetchingListSearch.value = Status.ERROR;
      xLogger.logMe(log: 'getData = ERROR ${e}', tLog: TLog.E);
    }
  }

  addToWatchlist()async{
    try {
      isFetchingAddToWatchlist.value = Status.LOADING;

      ApiBaseHelper _helper = ApiBaseHelper();
      var response = await _helper.post(BaseUrl.BASE_URL_API, BaseUrlHelper.uriAddToWatchlist(), jsonEncode(reqBodyAddToWatchlist));

      xLogger.logMe(log: 'add watchlist = State ${jsonEncode(reqBodyAddToWatchlist.value)} resp = ${response}',tLog: TLog.I);
      // if(response['success']!){
      //   isFetchingAddToWatchlist.value = Status.COMPLETED;
      //   xLogger.logMe(log: 'add watchlist = SUCCESS ${response}',tLog: TLog.I);
      //   Get.snackbar('Sukses', '${response['status_message']}', backgroundColor: Colors.green, colorText: Colors.black);
      // }else{
      //   isFetchingAddToWatchlist.value = Status.ERROR;
      //   xLogger.logMe(log: 'add watchlist = ERROR ${response}',tLog: TLog.E);
      //   Get.snackbar('Gagal', '${response['status_message']}', backgroundColor: Colors.red, colorText: Colors.white);
      // }
    } catch (e) {
      xLogger.logMe(log: 'add watchlist = ERROR $e',tLog: TLog.E);
      isFetchingAddToWatchlist.value = Status.ERROR;
    }
  }
}