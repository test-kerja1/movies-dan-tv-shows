import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kmovies/helpers/drawable.dart';

class WgtGlobal {
  Widget imageCache(
      {required String imgUrl,
        required ScreenUtil dimens,
        required double width,
        required double height,
        required double radius,
        BoxFit boxFit = BoxFit.cover}) {
    return Container(
      child: CachedNetworkImage(
        imageUrl: imgUrl,
        imageBuilder: (context, imageProvider) => Container(
          width: dimens.setWidth(width),
          height: dimens.setHeight(height),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              image: DecorationImage(
                image: imageProvider,
                fit: boxFit,
                colorFilter: ColorFilter.mode(
                  Colors.transparent,
                  BlendMode.colorBurn,
                ),
              )),
        ),
        placeholder: (context, url) => ClipRRect(
            borderRadius: BorderRadius.circular(radius),
            child: Container(
                width: dimens.setWidth(width),
                height: dimens.setHeight(height),
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.grey.withOpacity(0.1)),
                  backgroundColor: Color(0xFFF2FEFF),
                ))),
        errorWidget: (context, url, error) => ClipRRect(
            borderRadius: BorderRadius.circular(radius),
            child: Container(
                width: dimens.setWidth(width),
                height: dimens.setHeight(height),
                child: Image.asset(DrawableX.imageAsset(AssetGambar.null_image)))),
        fadeOutDuration: const Duration(seconds: 1),
        fadeInDuration: const Duration(seconds: 3),
      ),
    );
  }
}

final wgtGlobal = WgtGlobal();
