import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:kmovies/controller/kmovies_home_controller.dart';
import 'package:kmovies/helpers/widget/widget_global.dart';
import 'package:kmovies/models/Request/ReqModelsAddToWatchlist.dart';
import 'package:kmovies/models/Response/ResModelsNowPlayingMovies.dart';
import 'package:kmovies/models/Response/ResModelsPopularMovies.dart';
import 'package:kmovies/models/Response/ResModelsPopularTvShows.dart';
import 'package:kmovies/models/Response/ResModelsTopRatedMovies.dart';
import 'package:kmovies/models/Response/ResModelsTopRatedTvShows.dart';
import 'package:kmovies/models/Response/ResModelsUpcomingMovies.dart';

class MoviesTopRatedItem extends StatelessWidget {
  final ResultsTopRated? listData;
  final int? index;
  final int? lastIndex;
  final ScreenUtil? dimens;
  final String? title;
  final Function? onTap;
  final bool? listView;

  const MoviesTopRatedItem(
      {Key? key,
      @required this.listData,
      @required this.index,
      this.lastIndex,
      @required this.dimens,
      @required this.title,
      @required this.onTap,
      this.listView = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap!(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        width: dimens!.setWidth(140.0),
        margin: EdgeInsets.only(
            top: dimens!.setHeight(10.0),
            left: listView!
                ? (index == 0 ? dimens!.setWidth(10.0) : dimens!.setWidth(10.0))
                : dimens!.setWidth(10.0),
            right: listView!
                ? (lastIndex == index! + 1
                    ? dimens!.setWidth(10.0)
                    : dimens!.setWidth(0.0))
                : dimens!.setWidth(0.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                children: [
                  WgtGlobal().imageCache(
                      imgUrl:
                          'https://image.tmdb.org/t/p/original${listData!.posterPath}',
                      dimens: dimens!,
                      width: 130.0,
                      height: 160.0,
                      radius: 10.0),
                  GestureDetector(
                      onTap: () {
                        dialogWatchlist(context);
                      },
                      behavior: HitTestBehavior.opaque,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding:
                              EdgeInsets.only(right: dimens!.setWidth(15.0)),
                          child:
                              Icon(Icons.remove_red_eye, color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
            Container(
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: dimens!.setWidth(2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    ),
                    Text(
                      listData!.title!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Nunito', fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: dimens!.setHeight(5.0)),
                      child: Text(
                        listData!.releaseDate!.toString(),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.normal,
                            fontSize: dimens!.setSp(12.0),
                            color: Colors.grey.shade500),
                      ),
                    ),
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dialogWatchlist(context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: new Text("Confirmation", style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: dimens!.setSp(16.0)),),
            content: new Text("Add to Watchlist?", style: TextStyle(fontFamily: 'Nunito', fontSize: dimens!.setSp(14.0)),),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text("Cancel", style: TextStyle(color: Colors.grey)),
                onPressed: () {
                  Get.back();
                },
              ),
              new TextButton(
                child: new Text("Yapp"),
                onPressed: () async {
                  Get.back();
                  Get.find<KHomeController>().reqBodyAddToWatchlist.value = ReqModelsAddToWatchlist(
                    mediaId: listData!.id,
                    mediaType: "movie",
                      watchlist: true
                  );
                  Get.find<KHomeController>().addToWatchlist();
                },
              ),
            ],
          );
        });
  }
}

class MoviesUpcomingItem extends StatelessWidget {
  final ResultsUpcoming? listData;
  final int? index;
  final int? lastIndex;
  final ScreenUtil? dimens;
  final String? title;
  final Function? onTap;
  final bool? listView;

  const MoviesUpcomingItem(
      {Key? key,
      @required this.listData,
      @required this.index,
      this.lastIndex,
      @required this.dimens,
      @required this.title,
      @required this.onTap,
      this.listView = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap!(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        width: dimens!.setWidth(140.0),
        margin: EdgeInsets.only(
            top: dimens!.setHeight(10.0),
            left: listView!
                ? (index == 0 ? dimens!.setWidth(10.0) : dimens!.setWidth(10.0))
                : dimens!.setWidth(10.0),
            right: listView!
                ? (lastIndex == index! + 1
                    ? dimens!.setWidth(10.0)
                    : dimens!.setWidth(0.0))
                : dimens!.setWidth(0.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                children: [
                  WgtGlobal().imageCache(
                      imgUrl:
                          'https://image.tmdb.org/t/p/original${listData!.posterPath}',
                      dimens: dimens!,
                      width: 130.0,
                      height: 160.0,
                      radius: 10.0),
                  GestureDetector(
                      onTap: () {
                        dialogWatchlist(context);
                      },
                      behavior: HitTestBehavior.opaque,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding:
                          EdgeInsets.only(right: dimens!.setWidth(15.0)),
                          child:
                          Icon(Icons.remove_red_eye, color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
            Container(
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: dimens!.setWidth(2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    ),
                    Text(
                      listData!.title!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Nunito', fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: dimens!.setHeight(5.0)),
                      child: Text(
                        listData!.releaseDate!.toString(),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.normal,
                            fontSize: dimens!.setSp(12.0),
                            color: Colors.grey.shade500),
                      ),
                    ),
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dialogWatchlist(context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: new Text("Confirmation", style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: dimens!.setSp(16.0)),),
            content: new Text("Add to Watchlist?", style: TextStyle(fontFamily: 'Nunito', fontSize: dimens!.setSp(14.0)),),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text("Cancel", style: TextStyle(color: Colors.grey)),
                onPressed: () {
                  Get.back();
                },
              ),
              new TextButton(
                child: new Text("Yapp"),
                onPressed: () async {
                  Get.back();
                  Get.find<KHomeController>().reqBodyAddToWatchlist.value = ReqModelsAddToWatchlist(
                      mediaId: listData!.id,
                      mediaType: "movie",
                      watchlist: true
                  );
                  Get.find<KHomeController>().addToWatchlist();
                },
              ),
            ],
          );
        });
  }
}

class MoviesNowPlayingItem extends StatelessWidget {
  final ResultsNowPlaying? listData;
  final int? index;
  final int? lastIndex;
  final ScreenUtil? dimens;
  final String? title;
  final Function? onTap;
  final bool? listView;

  const MoviesNowPlayingItem(
      {Key? key,
      @required this.listData,
      @required this.index,
      this.lastIndex,
      @required this.dimens,
      @required this.title,
      @required this.onTap,
      this.listView = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap!(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        width: dimens!.setWidth(140.0),
        margin: EdgeInsets.only(
            top: dimens!.setHeight(10.0),
            left: listView!
                ? (index == 0 ? dimens!.setWidth(10.0) : dimens!.setWidth(10.0))
                : dimens!.setWidth(10.0),
            right: listView!
                ? (lastIndex == index! + 1
                    ? dimens!.setWidth(10.0)
                    : dimens!.setWidth(0.0))
                : dimens!.setWidth(0.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                children: [
                  WgtGlobal().imageCache(
                      imgUrl:
                          'https://image.tmdb.org/t/p/original${listData!.posterPath}',
                      dimens: dimens!,
                      width: 130.0,
                      height: 160.0,
                      radius: 10.0),
                  GestureDetector(
                      onTap: () {
                        dialogWatchlist(context);
                      },
                      behavior: HitTestBehavior.opaque,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding:
                          EdgeInsets.only(right: dimens!.setWidth(15.0)),
                          child:
                          Icon(Icons.remove_red_eye, color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
            Container(
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: dimens!.setWidth(2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    ),
                    Text(
                      listData!.title!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Nunito', fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: dimens!.setHeight(5.0)),
                      child: Text(
                        listData!.releaseDate!.toString(),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.normal,
                            fontSize: dimens!.setSp(12.0),
                            color: Colors.grey.shade500),
                      ),
                    ),
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dialogWatchlist(context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: new Text("Confirmation", style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: dimens!.setSp(16.0)),),
            content: new Text("Add to Watchlist?", style: TextStyle(fontFamily: 'Nunito', fontSize: dimens!.setSp(14.0)),),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text("Cancel", style: TextStyle(color: Colors.grey)),
                onPressed: () {
                  Get.back();
                },
              ),
              new TextButton(
                child: new Text("Yapp"),
                onPressed: () async {
                  Get.back();
                  Get.find<KHomeController>().reqBodyAddToWatchlist.value = ReqModelsAddToWatchlist(
                      mediaId: listData!.id,
                      mediaType: "movie",
                      watchlist: true
                  );
                  Get.find<KHomeController>().addToWatchlist();
                },
              ),
            ],
          );
        });
  }
}

class MoviesPopularItem extends StatelessWidget {
  final ResultsPopular? listData;
  final int? index;
  final int? lastIndex;
  final ScreenUtil? dimens;
  final String? title;
  final Function? onTap;
  final bool? listView;

  const MoviesPopularItem(
      {Key? key,
      @required this.listData,
      @required this.index,
      this.lastIndex,
      @required this.dimens,
      @required this.title,
      @required this.onTap,
      this.listView = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap!(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        width: dimens!.setWidth(140.0),
        margin: EdgeInsets.only(
            top: dimens!.setHeight(10.0),
            left: listView!
                ? (index == 0 ? dimens!.setWidth(10.0) : dimens!.setWidth(10.0))
                : dimens!.setWidth(10.0),
            right: listView!
                ? (lastIndex == index! + 1
                    ? dimens!.setWidth(10.0)
                    : dimens!.setWidth(0.0))
                : dimens!.setWidth(0.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                children: [
                  WgtGlobal().imageCache(
                      imgUrl:
                          'https://image.tmdb.org/t/p/original${listData!.posterPath}',
                      dimens: dimens!,
                      width: 130.0,
                      height: 160.0,
                      radius: 10.0),
                  GestureDetector(
                      onTap: () {
                        dialogWatchlist(context);
                      },
                      behavior: HitTestBehavior.opaque,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding:
                          EdgeInsets.only(right: dimens!.setWidth(15.0)),
                          child:
                          Icon(Icons.remove_red_eye, color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
            Container(
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: dimens!.setWidth(2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    ),
                    Text(
                      listData!.title!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Nunito', fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: dimens!.setHeight(5.0)),
                      child: Text(
                        listData!.releaseDate!.toString(),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.normal,
                            fontSize: dimens!.setSp(12.0),
                            color: Colors.grey.shade500),
                      ),
                    ),
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dialogWatchlist(context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: new Text("Confirmation", style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: dimens!.setSp(16.0)),),
            content: new Text("Add to Watchlist?", style: TextStyle(fontFamily: 'Nunito', fontSize: dimens!.setSp(14.0)),),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text("Cancel", style: TextStyle(color: Colors.grey)),
                onPressed: () {
                  Get.back();
                },
              ),
              new TextButton(
                child: new Text("Yapp"),
                onPressed: () async {
                  Get.back();
                  Get.find<KHomeController>().reqBodyAddToWatchlist.value = ReqModelsAddToWatchlist(
                      mediaId: listData!.id,
                      mediaType: "movie",
                      watchlist: true
                  );
                  Get.find<KHomeController>().addToWatchlist();
                },
              ),
            ],
          );
        });
  }
}

class MoviesPopularTvShowsItem extends StatelessWidget {
  final ResultsPopularTvShows? listData;
  final int? index;
  final int? lastIndex;
  final ScreenUtil? dimens;
  final String? title;
  final Function? onTap;
  final bool? listView;

  const MoviesPopularTvShowsItem(
      {Key? key,
      @required this.listData,
      @required this.index,
      this.lastIndex,
      @required this.dimens,
      @required this.title,
      @required this.onTap,
      this.listView = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap!(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        width: dimens!.setWidth(140.0),
        margin: EdgeInsets.only(
            top: dimens!.setHeight(10.0),
            left: listView!
                ? (index == 0 ? dimens!.setWidth(10.0) : dimens!.setWidth(10.0))
                : dimens!.setWidth(10.0),
            right: listView!
                ? (lastIndex == index! + 1
                    ? dimens!.setWidth(10.0)
                    : dimens!.setWidth(0.0))
                : dimens!.setWidth(0.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                children: [
                  WgtGlobal().imageCache(
                      imgUrl:
                          'https://image.tmdb.org/t/p/original${listData!.posterPath}',
                      dimens: dimens!,
                      width: 130.0,
                      height: 160.0,
                      radius: 10.0),
                  GestureDetector(
                      onTap: () {
                        dialogWatchlist(context);
                      },
                      behavior: HitTestBehavior.opaque,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding:
                          EdgeInsets.only(right: dimens!.setWidth(15.0)),
                          child:
                          Icon(Icons.remove_red_eye, color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
            Container(
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: dimens!.setWidth(2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    ),
                    Text(
                      listData!.name!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Nunito', fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: dimens!.setHeight(5.0)),
                      child: Text(
                        listData!.firstAirDate!.toString(),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.normal,
                            fontSize: dimens!.setSp(12.0),
                            color: Colors.grey.shade500),
                      ),
                    ),
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dialogWatchlist(context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: new Text("Confirmation", style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: dimens!.setSp(16.0)),),
            content: new Text("Add to Watchlist?", style: TextStyle(fontFamily: 'Nunito', fontSize: dimens!.setSp(14.0)),),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text("Cancel", style: TextStyle(color: Colors.grey)),
                onPressed: () {
                  Get.back();
                },
              ),
              new TextButton(
                child: new Text("Yapp"),
                onPressed: () async {
                  Get.back();
                  Get.find<KHomeController>().reqBodyAddToWatchlist.value = ReqModelsAddToWatchlist(
                      mediaId: listData!.id,
                      mediaType: "movie",
                      watchlist: true
                  );
                  Get.find<KHomeController>().addToWatchlist();
                },
              ),
            ],
          );
        });
  }
}

class MoviesTopRatedTvShowsItem extends StatelessWidget {
  final ResultsTopRatedTv? listData;
  final int? index;
  final int? lastIndex;
  final ScreenUtil? dimens;
  final String? title;
  final Function? onTap;
  final bool? listView;

  const MoviesTopRatedTvShowsItem(
      {Key? key,
      @required this.listData,
      @required this.index,
      this.lastIndex,
      @required this.dimens,
      @required this.title,
      @required this.onTap,
      this.listView = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap!(),
      behavior: HitTestBehavior.opaque,
      child: Container(
        width: dimens!.setWidth(140.0),
        margin: EdgeInsets.only(
            top: dimens!.setHeight(10.0),
            left: listView!
                ? (index == 0 ? dimens!.setWidth(10.0) : dimens!.setWidth(10.0))
                : dimens!.setWidth(10.0),
            right: listView!
                ? (lastIndex == index! + 1
                    ? dimens!.setWidth(10.0)
                    : dimens!.setWidth(0.0))
                : dimens!.setWidth(0.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Stack(
                children: [
                  WgtGlobal().imageCache(
                      imgUrl:
                          'https://image.tmdb.org/t/p/original${listData!.posterPath}',
                      dimens: dimens!,
                      width: 130.0,
                      height: 160.0,
                      radius: 10.0),
                  GestureDetector(
                      onTap: () {
                        dialogWatchlist(context);
                      },
                      behavior: HitTestBehavior.opaque,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          padding:
                          EdgeInsets.only(right: dimens!.setWidth(15.0)),
                          child:
                          Icon(Icons.remove_red_eye, color: Colors.white),
                        ),
                      )),
                ],
              ),
            ),
            Container(
              child: Container(
                padding:
                    EdgeInsets.symmetric(horizontal: dimens!.setWidth(2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    ),
                    Text(
                      listData!.name!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Nunito', fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: dimens!.setHeight(5.0)),
                      child: Text(
                        listData!.firstAirDate!.toString(),
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontFamily: 'Nunito',
                            fontWeight: FontWeight.normal,
                            fontSize: dimens!.setSp(12.0),
                            color: Colors.grey.shade500),
                      ),
                    ),
                    SizedBox(
                      height: dimens!.setHeight(10.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void dialogWatchlist(context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            title: new Text("Confirmation", style: TextStyle(fontFamily: 'Nunito', fontWeight: FontWeight.bold, fontSize: dimens!.setSp(16.0)),),
            content: new Text("Add to Watchlist?", style: TextStyle(fontFamily: 'Nunito', fontSize: dimens!.setSp(14.0)),),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              TextButton(
                child: Text("Cancel", style: TextStyle(color: Colors.grey)),
                onPressed: () {
                  Get.back();
                },
              ),
              new TextButton(
                child: new Text("Yapp"),
                onPressed: () async {
                  Get.back();
                  Get.find<KHomeController>().reqBodyAddToWatchlist.value = ReqModelsAddToWatchlist(
                      mediaId: listData!.id,
                      mediaType: "movie",
                      watchlist: true
                  );
                  Get.find<KHomeController>().addToWatchlist();
                },
              ),
            ],
          );
        });
  }
}
