import 'dart:io';
import 'package:device_info/device_info.dart';
enum pInfo { appName, packageName, version,buildNumber }
class DeviceConfig {
  getUserAgent() async {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await DeviceInfoPlugin().androidInfo;
        return "Android (" +
            androidInfo.brand +
            ", " +
            androidInfo.device +
            ", " +
            androidInfo.model +
            ", " +
            androidInfo.version.sdkInt.toString() +
            ")";
      } else {
        IosDeviceInfo iosInfo = await DeviceInfoPlugin().iosInfo;
        return iosInfo.model +
            " (" +
            Uri.encodeFull(iosInfo.name) +
            ", " +
            iosInfo.systemVersion +
            ")";
      }
    } catch (e) {
      if (Platform.isAndroid) {
        return "Android Unknown";
      } else if (Platform.isIOS) {
        return "iPhone Unknown";
      } else
        return "Unknown Devices";
    }
  }
}

var deviceConfig = DeviceConfig();
