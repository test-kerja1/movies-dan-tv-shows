import 'package:flutter/material.dart';

class Constants{


  final primaryColor = Color(0xFFF2DE1A);
  final secondaryColor = Color(0xFFF2B31A);
  final greyColor = Color(0xFF9e9e9e);

}
final constants = Constants();
