//Urutkan Berdasarkan Abjad A-Z
enum AssetGambar {
  logo,
  logo_dark,
  oval_bottom,
  null_image,
  watchlist,
  null_file,
}
class DrawableX{
  //Urutkan Berdasarkan Abjad A-Z
  static String imageAsset(AssetGambar assetGambar){
    switch (assetGambar){
      case AssetGambar.logo                 : return "assets/logo/logo.png";
      case AssetGambar.logo_dark            : return "assets/logo/logo-dark.png";
      case AssetGambar.oval_bottom          : return "assets/component/oval.png";
      case AssetGambar.null_image           : return "assets/illustration/null-image.png";
      case AssetGambar.watchlist            : return "assets/illustration/watchlist.png";
      case AssetGambar.null_file            : return "assets/illustration/null-file.png";
    }
  }
}
