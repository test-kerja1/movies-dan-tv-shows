import 'dart:math';

import 'package:kmovies/config/logger.dart';
import 'package:get_storage/get_storage.dart';

class GetStoragePref{
  final box = GetStorage();

  //GetStorage
  final boxRequestToken="token";
  final boxLogin="login";
  final boxNmuser="nameusr";
  final boxTataTertib="tata";
  final boxSessionId="sessionid";
  final boxGuestSessionId="guestsessionid";
  final boxIduser="userid";
  final boxSplash="splash";

  final boxRw= "box_rwuser";


  String? getToken() {
    try{
      xLogger.logMe(log: 'Token : ${box.read(boxRequestToken)}');
      return box.read(boxRequestToken) == null ? 'token' :  box.read(boxRequestToken) ;
    }catch(e){
      return 'token';
    }

  }

  void setToken(String? value) {
    if(value != null) {
      box.write(boxRequestToken, value);
    }
  }

  String? getNameUser() {
    try{
      xLogger.logMe(log: 'Token : ${box.read(boxNmuser)}');
      return box.read(boxNmuser) == null ? 'token' :  box.read(boxNmuser) ;
    }catch(e){
      return 'token';
    }

  }

  void setNameUser(String? value) {
    if(value != null) {
      box.write(boxNmuser, value);
    }
  }

  bool? getLogin() {
    try{
      return box.read(boxLogin) == null ? false :  box.read(boxLogin) ;
    }catch(e){
      return false;
    }
  }

  void setLogin(bool value) {
    box.write(boxLogin, value);
  }

  bool? getSplash() {
    try{
      return box.read(boxSplash) == null ? false :  box.read(boxSplash) ;
    }catch(e){
      return false;
    }
  }

  void setSplash(bool value) {
    box.write(boxSplash, value);
  }

  String? getIdUser() {
    try{

      return box.read(boxIduser) == null ? '' :  box.read(boxIduser) ;
    }catch(e){
      return '';
    }
  }

  void setIdUser(String? value) {
    if(value != null) {
      box.write(boxIduser, value);
    }
  }

  String? getSessionId() {
    try{

      return box.read(boxSessionId) == null ? '' :  box.read(boxSessionId) ;
    }catch(e){
      return '';
    }
  }

  void setSessionId(String? value) {
    if(value != null) {
      box.write(boxSessionId, value);
    }
  }

  String? getGuestSessionId() {
    try{

      return box.read(boxGuestSessionId) == null ? '' :  box.read(boxGuestSessionId) ;
    }catch(e){
      return '';
    }
  }

  void setGuestSessionId(String? value) {
    if(value != null) {
      box.write(boxGuestSessionId, value);
    }
  }

  void putCache(String key, String value, int jam) {
    print( 'putCache : ' + key+ ' '+ jam.toString() + ' : '+value);
    box.write(key, value);
    //tanggal disimpan
    box.write(key + "LastUpdate", DateTime.now().millisecondsSinceEpoch);
    //hitungan jam
    box.write(key + "Lama", jam * 60 * 60 * 1000);
  }

  String? getCache(String key) {
    print( 'getCache : ' + key);
    if (!box.hasData(key)) {
      print( 'getCache : hasData ' + key);
      return '';
    }
    int now = DateTime.now().millisecondsSinceEpoch;
    int last = box.read(key + "LastUpdate") ?? 0;
    int lama = box.read(key + "Lama") ?? 0;
    int rem = now - last;
    if (rem > lama) {
      print( 'getCache : Expired ' + key);
      return '';
    }
    print( 'getCache : Ada Data ' + key);
    return box.read(key);
  }

  /*
   * Saat logout, panggil ini untuk hapus data
   */
  void logout() {
    box.erase();
  }
}
final getBox = GetStoragePref();
