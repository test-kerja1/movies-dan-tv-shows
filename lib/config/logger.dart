
import 'package:kmovies/helpers/device_config.dart';
import 'package:logger/logger.dart';

import 'package:kmovies/services/base_url.dart';

enum TLog { V,E,I }
class BlocLogger {
  var tag = "KMoviesLog";

  logMe({String log="",TLog tLog = TLog.V}) async {
    var modeDev = false;
    if (BaseUrlHelper.dev || modeDev) {

      var deviceName = await deviceConfig.getUserAgent();

      var lognya = tag+'v1 => { $deviceName } => $log';

      var logger = Logger(
        printer: PrettyPrinter(
            methodCount: 2, // number of method calls to be displayed
            errorMethodCount: 8, // number of method calls if stacktrace is provided
            lineLength: 120, // width of the output
            colors: true, // Colorful log messages
            printEmojis: true, // Print an emoji for each log message
            printTime: false // Should each log print contain a timestamp
        ),
      );
      switch(tLog){
        case TLog.E: logger.e(lognya);break;
        case TLog.V: logger.i(lognya);break;
        case TLog.I: logger.w(lognya);break;
      }
      return "Ok";
    }else{
      return "";
    }

  }
}
final xLogger = BlocLogger();
