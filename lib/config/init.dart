import 'package:get/get.dart';
import 'package:kmovies/controller/kmovies_home_controller.dart';

class InitBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(KHomeController());
  }

}
