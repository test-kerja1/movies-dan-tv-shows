import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:kmovies/view/global/bottom_nav.dart';
import 'package:kmovies/view/global/kmovies_splash.dart';

import 'config/init.dart';

void main() async {
  runApp(
    ScreenUtilInit(
      designSize: Size(360, 690),
      builder: (BuildContext context, child) => GetMaterialApp(
        debugShowCheckedModeBanner: true,
        // getPages: [GetPage(name: '/nav', page: () => BottomNavi())],
        home: KMoviesSplash(),
        initialBinding: InitBinding(),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        theme: ThemeData(fontFamily: 'Nunito'),
        supportedLocales: [
          const Locale('en', 'US'), // English
          const Locale('id', 'ID'), // Indonesia
        ],
      ),
    ),
  );
}
