class ReqModelsValidToken {
  ReqModelsValidToken({
      this.username, 
      this.password, 
      this.requestToken,});

  ReqModelsValidToken.fromJson(dynamic json) {
    username = json['username'];
    password = json['password'];
    requestToken = json['request_token'];
  }
  String? username;
  String? password;
  String? requestToken;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['username'] = username;
    map['password'] = password;
    map['request_token'] = requestToken;
    return map;
  }

}