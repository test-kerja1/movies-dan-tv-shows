class ReqModelsSession {
  ReqModelsSession({
      this.requestToken,});

  ReqModelsSession.fromJson(dynamic json) {
    requestToken = json['request_token'];
  }
  String? requestToken;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['request_token'] = requestToken;
    return map;
  }

}