class ResModelsSearchMoviesTvShows {
  ResModelsSearchMoviesTvShows({
      this.page, 
      this.results, 
      this.totalPages, 
      this.totalResults,});

  ResModelsSearchMoviesTvShows.fromJson(dynamic json) {
    page = json['page'];
    if (json['results'] != null) {
      results = [];
      json['results'].forEach((v) {
        results?.add(ResultsSearch.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }
  int? page;
  List<ResultsSearch>? results;
  int? totalPages;
  int? totalResults;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['page'] = page;
    if (results != null) {
      map['results'] = results?.map((v) => v.toJson()).toList();
    }
    map['total_pages'] = totalPages;
    map['total_results'] = totalResults;
    return map;
  }

}

class ResultsSearch {
  ResultsSearch({
      this.adult, 
      this.backdropPath, 
      this.genreIds, 
      this.id, 
      this.mediaType, 
      this.originalLanguage, 
      this.originalTitle, 
      this.overview, 
      this.popularity, 
      this.posterPath, 
      this.releaseDate, 
      this.title, 
      this.video,
      this.voteCount,});

  ResultsSearch.fromJson(dynamic json) {
    adult = json['adult'];
    backdropPath = json['backdrop_path'];
    // if (json['genre_ids'] != null) {
    //   genreIds = [];
    //   json['genre_ids'].forEach((v) {
    //     genreIds?.add(Dynamic.fromJson(v));
    //   });
    // }
    id = json['id'];
    mediaType = json['media_type'];
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    overview = json['overview'];
    popularity = json['popularity'];
    posterPath = json['poster_path'];
    releaseDate = json['release_date'];
    title = json['title'];
    video = json['video'];
    voteCount = json['vote_count'];
  }
  bool? adult;
  dynamic backdropPath;
  List<dynamic>? genreIds;
  int? id;
  String? mediaType;
  String? originalLanguage;
  String? originalTitle;
  String? overview;
  double? popularity;
  dynamic posterPath;
  String? releaseDate;
  String? title;
  bool? video;
  int? voteCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['adult'] = adult;
    map['backdrop_path'] = backdropPath;
    if (genreIds != null) {
      map['genre_ids'] = genreIds?.map((v) => v.toJson()).toList();
    }
    map['id'] = id;
    map['media_type'] = mediaType;
    map['original_language'] = originalLanguage;
    map['original_title'] = originalTitle;
    map['overview'] = overview;
    map['popularity'] = popularity;
    map['poster_path'] = posterPath;
    map['release_date'] = releaseDate;
    map['title'] = title;
    map['video'] = video;
    map['vote_count'] = voteCount;
    return map;
  }

}