import 'dart:io';
import 'package:kmovies/services/app_exceptions.dart';
import 'package:kmovies/services/base_url.dart';
import 'package:kmovies/config/logger.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class ApiBaseHelper {
  Future<dynamic> get(BaseUrl baseUrl,  String url) async {
    final Urlx = BaseUrlHelper.getValue(baseUrl) + url;
    final token = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0OTZiNjM5MDExN2I0MzliZGI0YjU2YmYyMDQxY2JkMiIsInN1YiI6IjYyOWYzYTE2ZGMxY2I0NTJlMDA0YjU1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.phjOEuvnMtVPEjcJBsVGLAZISywOfkdwcOZv5T27BeM';
    final key = '496b6390117b439bdb4b56bf2041cbd2';

    var responseJson;

    Map<String, String>? headers = {
      "Accept": "application/json",
      "Authorization": "Bearer $token",
      "Content-Type" : "application/json;charset=utf-8",
    };

    try {
      xLogger.logMe(log:'Api Get, url $Urlx Headers : ' + headers.toString());
      final response = await http.get(Uri.parse(Urlx), headers: headers).timeout(const Duration(seconds: 60));
      responseJson = _returnResponse(response);
    } on SocketException {
      xLogger.logMe(log:'No net');
      throw FetchDataException('No Internet connection');
    }
    xLogger.logMe(log:'api get recieved!');
    return responseJson;
  }

  Future<dynamic> post(BaseUrl baseUrl,  String url, dynamic body) async {
    final Urlx = BaseUrlHelper.getValue(baseUrl) + url;

    Map<String, String>? myHeaders;
    if(baseUrl == BaseUrl.BASE_URL_LOG){
      myHeaders = {'Content-Type': 'text/plain; charset=UTF-8'};
    }else{
      myHeaders = {
        'Content-Type': 'application/json;charset=utf-8'
      };
    }

    if(baseUrl != BaseUrl.BASE_URL_LOG) {
      xLogger.logMe(log:'Api Post, url ${BaseUrlHelper.getValue(baseUrl)}$url Header:  ${myHeaders.toString()}');
    }
    var responseJson;
    try {
      final response = await http.post(
          Uri.parse(Urlx),
          headers: myHeaders,
          body: body).timeout(const Duration(seconds: 60));
      responseJson = _returnResponse(response);
    } on SocketException {
      //xLogger.logMe(log:'No net');
      throw FetchDataException('No Internet connection');
    }
    //xLogger.logMe(log:'api post.');
    return responseJson;
  }

  Future<dynamic> put(BaseUrl baseUrl,  String url, dynamic body) async {
    final Urlx = BaseUrlHelper.getValue(baseUrl) + url;

    Map<String, String>? myHeaders;
    if(baseUrl == BaseUrl.BASE_URL_LOG){
      myHeaders = {'Content-Type': 'text/plain; charset=UTF-8'};
    }else{
      myHeaders = {
        'Content-Type': 'application/json'
      };
    }

    if(baseUrl != BaseUrl.BASE_URL_LOG) {
      xLogger.logMe(log:'Api Post, url ${BaseUrlHelper.getValue(baseUrl)}$url Header:  ${myHeaders.toString()}');
    }
    var responseJson;
    try {
      final response = await http.put(
          Uri.parse(Urlx),
          headers: myHeaders,
          body: body).timeout(const Duration(seconds: 60));
      responseJson = _returnResponse(response);
    } on SocketException {
      //xLogger.logMe(log:'No net');
      throw FetchDataException('No Internet connection');
    }
    //xLogger.logMe(log:'api post.');
    return responseJson;
  }

  Future<dynamic> patch(BaseUrl baseUrl,  String url, dynamic body) async {
    final Urlx = BaseUrlHelper.getValue(baseUrl) + url;

    Map<String, String>? myHeaders;
    if(baseUrl == BaseUrl.BASE_URL_LOG){
      myHeaders = {'Content-Type': 'text/plain; charset=UTF-8'};
    }else{
      myHeaders = {
        'Content-Type': 'application/json'
      };
    }

    if(baseUrl != BaseUrl.BASE_URL_LOG) {
      xLogger.logMe(log:'Api Post, url ${BaseUrlHelper.getValue(baseUrl)}$url Header:  ${myHeaders.toString()}');
    }
    var responseJson;
    try {
      final response = await http.patch(
          Uri.parse(Urlx),
          headers: myHeaders,
          body: body).timeout(const Duration(seconds: 60));
      responseJson = _returnResponse(response);
    } on SocketException {
      //xLogger.logMe(log:'No net');
      throw FetchDataException('No Internet connection');
    }
    //xLogger.logMe(log:'api post.');
    return responseJson;
  }

  Future<dynamic> delete(BaseUrl baseUrl, String url) async {
    final Urlx = BaseUrlHelper.getValue(baseUrl)+url;

    //xLogger.logMe(log:'Api Get, url $Urlx$url   api_app : $api_app  x_access : $x_access Token: ' + _accessToken.toString());

    Map<String, String>? headers = {
      "Accept": "application/json",
      "Content-Type" : "application/json",
    };
    var apiResponse;
    try {
      final response = await http.delete(Uri.parse(Urlx),headers: headers).timeout(const Duration(seconds: 60));
      apiResponse = _returnResponse(response);
    } on SocketException {
      //xLogger.logMe(log:'No net');
      throw FetchDataException('No Internet connection');
    }
    //xLogger.logMe(log:'api delete.');
    return apiResponse;
  }
}

dynamic _returnResponse(http.Response response) {
  try {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        // xLogger.logMe(log:'res200 : ' + responseJson.toString());
        return responseJson;
      case 400:
        var responseJson = json.decode(response.body.toString());
        //xLogger.logMe(log:'res400 : ' + responseJson.toString());
        return json.decode(response.body.toString());
      case 401:
      case 403:
      // xLogger.logMe(log:'res403 : ' + response.body.toString());
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException('Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }catch(e){
    //xLogger.logMe(log:'ErrorRestAPI ${response.statusCode}:-> ' + e.toString());
  }
}
