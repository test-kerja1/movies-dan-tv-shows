import 'package:kmovies/config/logger.dart';
import 'package:get/get.dart';
import 'package:kmovies/helpers/get_storage.dart';

enum BaseUrl { BASE_URL_API, BASE_URL_LOG }
enum Status { LOADING, COMPLETED, ERROR, NETRAL, MORE }
class BaseUrlHelper {
  //Change dev and staging false to prod
  static bool dev = true;
  static bool staging = false;

  static String getValue(BaseUrl baseUrl) {
    switch(baseUrl){
      case BaseUrl.BASE_URL_API:
        return dev ? staging ? "https://api.themoviedb.org/" : "https://api.themoviedb.org/" : "https://api.themoviedb.org/";
        break;
      case BaseUrl.BASE_URL_LOG:
        return dev ? staging ?  ""       : ""   : "";
        break;
    }
    //-------------------------Staging-----------------------------------DEV----------------------------prod-----------
  }

  static String getEnv() {
    return dev ? staging ? ".:STAGING:." : ".:DEV:." : "";
  }

  static String API_VERSION = "3";
  static String api_key = "496b6390117b439bdb4b56bf2041cbd2";

  //First Run
  static String uriGetRequestToken() => "${API_VERSION}/authentication/token/new?api_key=${api_key}";
  static String uriGetValidToken() => "${API_VERSION}/authentication/token/validate_with_login?api_key=${api_key}";
  static String uriGetSession() => "${API_VERSION}/authentication/session/new?api_key=${api_key}";
  static String uriGetGuestSession() => "${API_VERSION}/authentication/guest_session/new?api_key=${api_key}";
  static String uriGetAccountId() => "${API_VERSION}/account?api_key=${api_key}&session_id=${getBox.getSessionId()}";

  //Home
  static String uriGetTopRatedMovies(page) => "${API_VERSION}/movie/top_rated?api_key=${api_key}&language=en-US&page=$page";
  static String uriGetUpcomingMovies(page) => "${API_VERSION}/movie/upcoming?api_key=${api_key}&language=en-US&page=$page";
  static String uriGetNowPlayingMovies(page) => "${API_VERSION}/movie/now_playing?api_key=${api_key}&language=en-US&page=$page";
  static String uriGetPopularMovies(page) => "${API_VERSION}/movie/popular?api_key=${api_key}&language=en-US&page=$page";
  static String uriGetPopularTvShows(page) => "${API_VERSION}/tv/popular?api_key=${api_key}&language=en-US&page=$page";
  static String uriGetTopRatedTvShows(page) => "${API_VERSION}/tv/top_rated?api_key=${api_key}&language=en-US&page=$page";
  static String uriGetSearch(cari) => "${API_VERSION}/search/multi?api_key=${api_key}&language=en-US&page=1&include_adult=false&query=$cari";

  static String uriAddToWatchlist() => "${API_VERSION}/account/${getBox.getIdUser()}/watchlist?api_key=${api_key}&session_id=${getBox.getSessionId()}";
}
